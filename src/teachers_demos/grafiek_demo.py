import pylab as pl
golflengte, relatieve_flux = pl.loadtxt('Voorbeeld1.data', 
        usecols=(0,1), unpack=True)
pl.plot(golflengte, relatieve_flux, 'o')
pl.xlabel('Golflengte (Angstrom)')
pl.ylabel('Relatieve Flux')
pl.show()
