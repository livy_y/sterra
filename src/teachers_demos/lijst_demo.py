import pylab as pl
import numpy as np
import glob

# Een lijst creeren en een element van de lijst kiezen
lijst = np.array([1.0, 2.0, 3.0, 4.0])
print(lijst[0])

# Enkele elementen van de lijst kiezen
print(lijst[1:3])
print(lijst[:3])
print(lijst[1:])

# Som van elementen in de lijst berekenen
lijstsom = lijst.sum()
print(lijstsom)

# Bijkomende elementen aan de lijst toevoegen
leeglijst = np.array([])
leeglijst = np.append(leeglijst, lijst)
print(leeglijst)

# Elementen van de lijst aan elkaar toevoegen
lijst2 = (lijst + lijst) / 2
lijst3 = np.add(lijst, lijst) / 2
print(lijst2, lijst3)

# Over de elementen van de lijst lussen met een for lus
for element in lijst:
    print(element)

# For lus met range() gebruiken om de x- en y-waarden berekenen
x = np.array([])
y = np.array([])
for i in range(0, 10):
    x = np.append(x, i)
    y = np.append(y, i**2)
print(x, y)
pl.plot(x, y, 'o')
pl.show()

# Een lijst van bestandsnamen maken
bestandslijst = glob.glob("*.fits")
print(bestandslijst[0:5])