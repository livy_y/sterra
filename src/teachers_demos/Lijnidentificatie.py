# om een plot te maken van een spectrum rond het koolstofmultiplet

import sys
import glob
import os
import pylab as pl
import astropy.io.fits as pf
import numpy as np
from scipy import interpolate

from astropy import constants as const
from scipy.optimize import leastsq

args = sys.argv

pl.rcParams['ps.usedistiller'] = 'xpdf'
pl.rcParams['font.size'] = 15
pl.rcParams['font.weight'] = 'black'
pl.rcParams['legend.fontsize'] = 15

##
## inlezen van alle data in een lijst door gebruik te maken van een wildcard (*)
##

datalistc = glob.glob("./assets/doublestar_measurements/*_HRF_OBJ_ext_CosmicsRemoved_log_merged_c.fits")
#print(datalistc)

##
## we nemen spectrum in cel 30 (willekeurige keuze)
##

print(len(datalistc))

specc = pf.getdata(datalistc[10])
headerc = pf.getheader(datalistc[10])


##
## we maken een golflengtegrid
##
ref_pixc = int(headerc['CRPIX1'])-1
ref_valc = float(headerc['CRVAL1'])
ref_delc = float(headerc['CDELT1'])
numberpointsc = specc.shape[0]
unitxc = headerc['CTYPE1']
wavelengthbeginc = ref_valc - ref_pixc*ref_delc
wavelengthendc = wavelengthbeginc + (numberpointsc-1)*ref_delc
wavelengthsc = pl.linspace(wavelengthbeginc,wavelengthendc,numberpointsc)
wavelengthsc = np.exp(wavelengthsc)


#
# datum van de waarneming
#
JD = headerc['BJD']

#
# inlezen van de spectrale lijnlijst. De golflengte is in rust en in lucht (niet vacuum)
#

inputfile = "./assets/doublestar_measurements/LijnLijst.data"
lines,weights = np.loadtxt(inputfile,usecols=(0,1),unpack=True)
print(weights)

pl.plot(wavelengthsc,specc)

#
# we centreren rond 7090 en 7130 en normeren daar door te delen door de mediaan binnen dat golflengtegebied
#
wavea = 7090.
waveb = 7130.


indices = np.where((wavelengthsc > wavea) & (wavelengthsc < waveb))
fluxb = np.median(specc[indices])

#
# Plot het spectrum door gebruik te maken van pylab
#
pl.title("Spectrum BD+39.4926")
pl.plot(wavelengthsc,specc/fluxb,'b')
pl.xlabel('wavelength (angstrom)')
pl.ylabel('spectrum')
pl.xlim((wavea,waveb))
pl.ylim((0.3,1.1))
dots=lines-lines+0.6
linesshift = lines + -50/300000.*lines
pl.plot(linesshift,dots,'o')
pl.annotate("-50 km/s",(7093,0.65))
pl.show()

