import numpy
from matplotlib import pyplot as plot
import astropy.io.fits as fit

SOURCE_FILE = "./assets/doublestar_measurements/00272473_HRF_OBJ_ext_CosmicsRemoved_log_merged_c.fits"

### Get data out of files ###
#############################

spectrum = fit.getdata(SOURCE_FILE)
header = fit.getheader(SOURCE_FILE)

### Process header ###
######################

ref_pix = int(header['CRPIX1'])-1
ref_val = float(header['CRVAL1'])
ref_del = float(header['CDELT1'])


numberpoints = spectrum.shape[0]
golflengtebegin = ref_val - ref_pix*ref_del
golflengteeeind = golflengtebegin + (numberpoints-1)*ref_del
golflengtesln = plot.linspace(golflengtebegin, golflengteeeind, numberpoints)
golflengtes = np.exp(golflengtesln)

pl.title("Spectrum rond Koolstof")
pl.plot(golflengtes, spectrum, 'o')
pl.xlabel('Golflengte (Angstrom)')
pl.ylabel('Relatieve Flux')
pl.xlim(7090,7130)
pl.show()
