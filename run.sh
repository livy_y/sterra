virtualenv -p python3.10 venv
source ./venv/bin/activate
pip install --upgrade pip
pip install -r ./requirements.txt
python3 ./src/main.py
